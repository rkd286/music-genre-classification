# Music Genre Classification

* **Dataset:** FMA small (https://github.com/mdeff/fma)

* **No. of observations/audio files:** 8000 (30 seconds each)

* **No. of classes/genres:** 8 (Electronic, Experimental, Folk, Hip-Hop, Instrumental, International, Pop, Rock)

* **Parameters:**

    - **Window Length (Fast Fourier Transform Window):** 2048
    - **Hop Length:** 1024 
    - **Sampling Rate:** 22.1KHz



## Setup

> Download the raw audio files(fma_small) and metadata from [here](https://github.com/mdeff/fma)

```
pip install -r requirements.txt
```

## Introduction

Music streaming platforms like Spotify and Soundcloud are investing millions to research on improving the way users find and listen to music. Recommendation Systems plays a vital role in User Experience for any application. Companies use Machine Learning/Deep Learning to classify music genres to place recommendations to their customers.
As part of this Music Analysis, we will see how to classify music genres using a challenging dataset (FMA-small). Two techniques were experimented as part of this analysis.
* CNN (Using Spectrogram image)
* CNN – RNN (Using Mel Spectrogram)

## Terminologies

1)	**Sampling Rate:** The number of samples per second of time series. It is measured in Hz (or KHz)

2)	**Hop Length:** The number of samples between successive frames, e.g., the columns of a spectrogram.

3)	**Frame:** A short slice of a time series used for analysis purposes. This usually corresponds to a single column of a spectrogram matrix.

4)	**Fast Fourier Transform (FFT):** The FFT is an important measurement method in audio and acoustics measurements. It converts the signal into spectral components and thereby provides frequency information about the signal.

5)	**FFT Window:** An FFT window is a contiguous segment of audio samples on which the transform is applied.

6)	**Mel Spectrogram:** A regular spectrogram is squashed using the mel scale to convert the audio frequencies into something more human readable.

## Visualizing Audio

### 1. Waveform
A typical audio signal can be expressed as a function of Time vs Amplitude.

![Waveform](./sample/sample_waveform.png)

### 2. Spectrogram

A spectrogram is a visual representation of the spectrum of frequencies of sound or other signals as they vary with time. Spectrograms are sometimes called sonographs, voiceprints, or voicegrams. In 2-dimensional arrays, the first axis is frequency while the second axis is time.

The spectrogram is key to our analysis. A regular spectrogram is the squared magnitude of the short term Fourier transform (STFT) of the audio signal. 

![Pop](./sample/sample_spec_pop.png)
![Hip-Hop](./sample/sample_spec_hiphop.png)

## CNN-RNN Model
Using Mel Spectrogram features

![CRNN](./sample/crnn.png)

### Usage

#### 1. Data Prepartion

```python
# If data has to be generated
python main.py prepare_data --model=crnn --load_pickle=False
```

#### 2. Train
```python
python main.py train --model=crnn

'''
Parameters (Optional)
--conv_layers            
--filters
--kernel_size
--dense_nodes
--batch_norm
--batch_norm_momentum
--pool
--lstm_nodes
--conv_dropout
--lstm_dropout
--dense_dropout
--activation
--l2
--learning_rate
--epochs
--batch_size
'''
```

## CNN Model
Using the Spectrogram Image

![CRNN](./sample/cnn.png)

### Usage

#### 1. Data Prepartion

```python
# If data has to be generated
python main.py prepare_data --model=cnn --load_pickle=False --generate_image=True
```

#### 2. Train
```python
python main.py train --model=cnn

'''
Parameters (Optional)
--activation
--learning_rate
--epochs
--batch_size
'''
```