import os
import numpy as np
import pandas as pd

from sklearn import preprocessing
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score

from tensorflow.keras.layers import Dense, Conv1D, LSTM, Activation, BatchNormalization, MaxPooling1D, Dropout, Input
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import regularizers
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.preprocessing import image
import tensorflow as tf

from data_engineering.crnn import get_data

def crnn_model(conv_layers=3, 
                filters = 56, 
                kernel_size = 5, 
                dense_nodes = 64,
                batch_norm=True, 
                batch_norm_momentum=0.9, 
                pool=2, 
                lstm_nodes=96, 
                conv_dropout=0.4, 
                lstm_dropout=0.4,
                dense_dropout=0.4, 
                activation='relu', 
                l2=0.001, 
                learning_rate=0.001,
                epochs=20,
                batch_size=256):

    X_train, y_train, X_val, y_val, X_test, y_test = get_data(load_pickle=True)
    
    model = Sequential()

    model.add(Input(shape=(None, X_train.shape[2])))

    for i in range(conv_layers):

        model.add(Conv1D(filters=filters, kernel_size=kernel_size, kernel_regularizer=regularizers.l2(l2)))
        model.add(BatchNormalization(momentum=batch_norm_momentum))
        model.add(Activation(activation))
        model.add(MaxPooling1D(pool))
        model.add(Dropout(conv_dropout))


    model.add(LSTM(lstm_nodes, return_sequences=False))
    model.add(Dropout(lstm_dropout))

    model.add(Dense(dense_nodes, activation=activation, kernel_regularizer=regularizers.l2(l2)))
    model.add(Dropout(dense_dropout))

    model.add(Dense(8, activation='softmax'))

    optimizer = Adam(learning_rate=learning_rate)

    model.compile(optimizer=optimizer,
                loss='sparse_categorical_crossentropy',
                metrics=['accuracy'])

    model.fit(X_train,
            y_train,
            epochs=epochs,
            batch_size=batch_size,
            validation_data=(X_val, y_val),
            )
    
    model.save('./saved/crnn.h5')

    preds = model.predict(X_test)
    print('accuracy on test data: {}'.foramt(accuracy_score(y_test, np.argmax(preds, axis=1))))
